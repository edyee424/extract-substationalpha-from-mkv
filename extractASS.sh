#/bin/sh

# This script extract SubStationAlpha subtitle from MKV file

# The for loop list all files starting with Still.17 in the current directory
for file in ./Still.17.*
do
  echo "$file"

  # Use egrep to extract episode number. i.e. E01, E02
  fileNum=$(echo $file | egrep -o -e '(E\d{2})')
  
  echo "$fileNum"
  
  # The mkvextract command extract track #2 to an ASS file. i.e. E01.ass, E02.ass
  # (use "mkvmerge -i <file name>" to find the track number)
  mkvextract tracks $file 2:"$fileNum".ass
done