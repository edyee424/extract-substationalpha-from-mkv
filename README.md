# extract-SubStationAlpha-from-MKV

Extract SubStationAlpha subtitle from a group of MKV files

## Description

When do you need this shell script?

I couldn't find appropriate subtitle files (mainly SRT) for the language that I want. As a result, I need to extract one of the existing subtitle from the MKV file. Then I use one of many online tools to translate the file.

I use MKVToolNix GUI to add the new subtitle file back to the MKV, and enerate a new MKV file. (I haven't figure out how to write script to automate this part yet)

## Dependencies

See the link in the Acknowledgement section.

## Installation

You can save the `extractASS.sh` to your local directory. Modify the shell script. Try it out.

The sample shell script will **NOT** delete file.


## Acknowledgement

The following link shows me how to use `mkvmerge` and `mkvextract`:

https://gist.github.com/pavelbinar/20a3366b54f41e355d2745c89091ec46
